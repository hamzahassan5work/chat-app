import 'dart:io';

import 'package:flutter/material.dart';

import 'adduserimage.dart';

class AuthForm extends StatefulWidget {
  final void Function(String email, String password) userLogin;
  final void Function(
      String email, String username, String password, File image) userSignUp;
  final bool isLoading;

  AuthForm({
    @required this.userLogin,
    @required this.userSignUp,
    @required this.isLoading,
  });

  @override
  _AuthFormState createState() => _AuthFormState();
}

class _AuthFormState extends State<AuthForm> {
  final _formKey = GlobalKey<FormState>();
  bool _isLoginMode = false;

  File _profilePhoto;
  String _email = '';
  String _username = '';
  String _password = '';

  void _photoSubmission(File image) {
    _profilePhoto = image;
  }

  void _formSubmission() {
    if (_formKey.currentState.validate()) {
      FocusScope.of(context).unfocus();
      _formKey.currentState.save();
      if (!_isLoginMode) {
        if (_profilePhoto != null) {
          widget.userSignUp(_email, _username, _password, _profilePhoto);
        } else {
          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            content: Text('Add Profile Photo'),
            duration: Duration(seconds: 2),
          ));
        }
      } else {
        widget.userLogin(_email, _password);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Card(
        margin: const EdgeInsets.all(10.0),
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: SingleChildScrollView(
            child: Form(
              key: _formKey,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  if (!_isLoginMode)
                    AddUserImage(_photoSubmission, widget.isLoading),
                  TextFormField(
                    enabled: !widget.isLoading,
                    key: ValueKey('email'),
                    decoration: InputDecoration(labelText: 'Email'),
                    keyboardType: TextInputType.emailAddress,
                    textInputAction: TextInputAction.next,
                    validator: (val) {
                      if (val.trim().isEmpty || !val.contains('@')) {
                        return 'Enter valid email address';
                      }
                      return null;
                    },
                    onSaved: (email) {
                      _email = email.trim();
                    },
                  ),
                  if (!_isLoginMode)
                    TextFormField(
                      enabled: !widget.isLoading,
                      key: ValueKey('username'),
                      maxLength: 25,
                      textCapitalization: TextCapitalization.words,
                      decoration: InputDecoration(labelText: 'Display Name'),
                      keyboardType: TextInputType.name,
                      textInputAction: TextInputAction.next,
                      validator: (val) {
                        if (val.trim().isEmpty) {
                          return 'Enter username';
                        }
                        return null;
                      },
                      onSaved: (username) {
                        _username = username.trim();
                      },
                    ),
                  TextFormField(
                    enabled: !widget.isLoading,
                    key: ValueKey('password'),
                    decoration: InputDecoration(labelText: 'Password'),
                    obscureText: true,
                    keyboardType: TextInputType.visiblePassword,
                    textInputAction: TextInputAction.done,
                    validator: (val) {
                      if (val.trim().isEmpty) {
                        return 'Enter password';
                      }
                      if (val.trim().length < 6) {
                        return 'Password must not less than 6 characters';
                      }
                      return null;
                    },
                    onSaved: (password) {
                      _password = password.trim();
                    },
                  ),
                  if (!widget.isLoading) SizedBox(height: 20),
                  if (widget.isLoading)
                    Container(
                      margin: EdgeInsets.all(15.0),
                      child: CircularProgressIndicator(),
                    ),
                  if (!widget.isLoading)
                    ElevatedButton(
                      child: _isLoginMode ? Text('Login') : Text('Sign up'),
                      onPressed: _formSubmission,
                    ),
                  if (!widget.isLoading)
                    TextButton(
                      child: _isLoginMode
                          ? Text('Not have an account yet?')
                          : Text('Already have an account?'),
                      onPressed: () {
                        setState(() {
                          _isLoginMode = !_isLoginMode;
                        });
                      },
                    ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
