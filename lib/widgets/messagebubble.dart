import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class MessageBubble extends StatelessWidget {
  final QueryDocumentSnapshot chatData;
  final Key key;

  MessageBubble(this.chatData, {this.key}) : super(key: key);

  final currentUID = FirebaseAuth.instance.currentUser.uid;

  @override
  Widget build(BuildContext context) {
    return chatData.data().containsKey('Newuser')
        ? Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              LimitedBox(
                maxWidth: MediaQuery.of(context).size.width * 0.80,
                child: Card(
                  margin: EdgeInsets.all(10.0),
                  elevation: 1,
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      currentUID == chatData['Uid']
                          ? 'You joined the room'
                          : chatData['Newuser'],
                      textAlign: TextAlign.center,
                      textWidthBasis: TextWidthBasis.longestLine,
                      style: TextStyle(
                        fontFamily: 'Quicksand',
                        color: Theme.of(context).primaryColor,
                      ),
                    ),
                  ),
                ),
              )
            ],
          )
        : Stack(
            alignment: Alignment.topLeft,
            children: [
              Padding(
                padding: EdgeInsets.only(
                    top: 6,
                    left: 18,
                    right: 0,
                    bottom: currentUID == chatData['Uid'] ? 0 : 8),
                child: Row(
                  mainAxisAlignment: currentUID == chatData['Uid']
                      ? MainAxisAlignment.end
                      : MainAxisAlignment.start,
                  children: [
                    LimitedBox(
                      maxWidth: MediaQuery.of(context).size.width * 0.75,
                      child: Card(
                        elevation: 0,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(12.0),
                          topRight: Radius.circular(12.0),
                          bottomLeft: currentUID == chatData['Uid']
                              ? Radius.circular(12.0)
                              : Radius.circular(0),
                          bottomRight: currentUID == chatData['Uid']
                              ? Radius.circular(0)
                              : Radius.circular(12.0),
                        )),
                        color: currentUID == chatData['Uid']
                            ? Colors.grey[300]
                            : Theme.of(context).accentColor,
                        child: Padding(
                          padding: EdgeInsets.only(
                              top: 5,
                              left: currentUID == chatData['Uid'] ? 10 : 24,
                              right: 10,
                              bottom: 5),
                          child: Column(
                            crossAxisAlignment: currentUID == chatData['Uid']
                                ? CrossAxisAlignment.end
                                : CrossAxisAlignment.start,
                            children: [
                              if (currentUID != chatData['Uid'])
                                Text(
                                  chatData['Username'],
                                  textWidthBasis: TextWidthBasis.longestLine,
                                  overflow: TextOverflow.ellipsis,
                                  textAlign: TextAlign.start,
                                  style: TextStyle(
                                    fontSize: 15,
                                    fontFamily: 'Quicksand',
                                    fontWeight: FontWeight.bold,
                                    color: Colors.amberAccent,
                                  ),
                                ),
                              if (currentUID != chatData['Uid'])
                                SizedBox(height: 4),
                              Text(
                                chatData['Text'],
                                textAlign: TextAlign.start,
                                textWidthBasis: TextWidthBasis.longestLine,
                                style: TextStyle(
                                  fontSize: 17,
                                  color: currentUID == chatData['Uid']
                                      ? Colors.black
                                      : Theme.of(context)
                                          .accentTextTheme
                                          .headline6
                                          .color,
                                ),
                              ),
                              SizedBox(height: 3),
                              Text(
                                '${DateFormat.yMMMd().format((chatData['Timestamp'] as Timestamp).toDate())} at ${DateFormat.Hm().format((chatData['Timestamp'] as Timestamp).toDate())}',
                                textAlign: currentUID == chatData['Uid']
                                    ? TextAlign.end
                                    : TextAlign.start,
                                style: TextStyle(
                                  fontFamily: 'Quicksand',
                                  color: currentUID == chatData['Uid']
                                      ? Colors.grey[600]
                                      : Colors.grey[400],
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              if (currentUID != chatData['Uid'])
                CircleAvatar(
                  backgroundColor: Colors.orangeAccent,
                  backgroundImage: NetworkImage(chatData['Userimage']),
                ),
            ],
          );
  }
}
