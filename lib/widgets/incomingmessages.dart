import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

import 'messagebubble.dart';

class IncomingMessages extends StatefulWidget {
  @override
  _IncomingMessagesState createState() => _IncomingMessagesState();
}

class _IncomingMessagesState extends State<IncomingMessages> {
  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      stream: FirebaseFirestore.instance
          .collection('chat')
          .orderBy('Timestamp', descending: true)
          .snapshots(),
      builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
        if (snapshot.hasError) {
          print(snapshot.error);
          return Center(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Text('Oops! Something Went Wrong :('),
                TextButton(
                  onPressed: () {
                    setState(() {});
                  },
                  child: Text('Try Again'),
                )
              ],
            ),
          );
        }
        if (snapshot.hasData) {
          if (snapshot.data.docs.isEmpty) {
            return Center(
              child: Text('No Accient Memories :('),
            );
          } else {
            return ListView.builder(
              reverse: true,
              itemCount: snapshot.data.docs.length,
              itemBuilder: (ctx, i) {
                return MessageBubble(
                  snapshot.data.docs[i],
                  key: ValueKey(snapshot.data.docs[i]['Uid']),
                );
              },
            );
          }
        }
        return Center(
          child: SizedBox(
            width: 50,
            child: LinearProgressIndicator(),
          ),
        );
      },
    );
  }
}
