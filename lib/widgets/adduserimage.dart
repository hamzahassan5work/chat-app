import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class AddUserImage extends StatefulWidget {
  final bool isLoading;
  final Function photoSubmission;

  AddUserImage(this.photoSubmission, this.isLoading);

  @override
  _AddUserImageState createState() => _AddUserImageState();
}

class _AddUserImageState extends State<AddUserImage> {
  File _pickedImage;

  void _selectImage() async {
    try {
      var pickedFile = await ImagePicker().getImage(
        source: ImageSource.gallery,
        imageQuality: 50,
        maxHeight: 150,
        maxWidth: 150,
      );
      if (pickedFile != null) {
        setState(() {
          _pickedImage = File(pickedFile.path);
          widget.photoSubmission(_pickedImage);
        });
      }
    } catch (e) {
      print(e);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 8.0),
      child: Stack(
        clipBehavior: Clip.none,
        children: [
          CircleAvatar(
            radius: 60,
            backgroundColor: Colors.blueGrey,
            backgroundImage:
                _pickedImage != null ? FileImage(_pickedImage) : null,
          ),
          Positioned(
            bottom: -3,
            right: -6,
            child: CircleAvatar(
              backgroundColor: Theme.of(context).primaryColor,
              child: FittedBox(
                child: IconButton(
                  onPressed: widget.isLoading ? null : _selectImage,
                  icon: Icon(
                    Icons.camera_alt,
                    color: Colors.white,
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
