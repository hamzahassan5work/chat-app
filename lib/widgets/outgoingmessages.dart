import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class OutgoingMessages extends StatefulWidget {
  @override
  _OutgoingMessagesState createState() => _OutgoingMessagesState();
}

class _OutgoingMessagesState extends State<OutgoingMessages> {
  final textController = TextEditingController();
  String userText = '';

  final db = FirebaseFirestore.instance.collection('chat');
  final userId = FirebaseAuth.instance.currentUser.uid;
  final username = FirebaseAuth.instance.currentUser.displayName;
  final userImage = FirebaseAuth.instance.currentUser.photoURL;

  void sendMessage() async {
    textController.clear();
    try {
      await db.add({
        'Text': userText,
        'Username': username,
        'Uid': userId,
        'Userimage': userImage,
        'Timestamp': Timestamp.now(),
      });
    } catch (e) {
      ScaffoldMessenger.of(context)
          .showSnackBar(SnackBar(content: Text(e.toString())));
    }
    setState(() => userText = '');
  }

  @override
  Widget build(BuildContext context) {
    return TextField(
      controller: textController,
      maxLines: null,
      textCapitalization: TextCapitalization.sentences,
      decoration: InputDecoration(
        hintText: 'Type a message',
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(12.0),
        ),
        suffixIcon: IconButton(
          icon: Icon(Icons.send_rounded),
          onPressed: userText.isEmpty ? null : sendMessage,
        ),
      ),
      onChanged: (val) {
        setState(() {
          userText = val.trim();
        });
      },
    );
  }
}
