import 'package:chatapp/screens/chatscreen.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';

import './screens/authscreen.dart';
import './screens/splashscreen.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: Firebase.initializeApp(),
      builder: (ctx, futureSnapshot) {
        if (futureSnapshot.hasData) {
          return MaterialApp(
            debugShowCheckedModeBanner: false,
            title: 'ChatInsta',
            theme: ThemeData(
              fontFamily: 'OpenSans',
              primarySwatch: Colors.pink,
              backgroundColor: Colors.pink,
              accentColor: Colors.deepPurpleAccent,
              accentColorBrightness: Brightness.dark,
              elevatedButtonTheme: ElevatedButtonThemeData(
                style: ButtonStyle(
                  shape: MaterialStateProperty.all(
                    RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20.0),
                    ),
                  ),
                ),
              ),
            ),
            home: FirebaseAuth.instance.currentUser == null
                ? AuthScreen()
                : ChatScreen(),
          );
        }
        if (futureSnapshot.hasError) {
          return MaterialApp(
            debugShowCheckedModeBanner: false,
            theme: ThemeData(
              fontFamily: 'Quicksand',
            ),
            title: 'ChatInsta',
            home: Scaffold(
              body: Padding(
                padding: const EdgeInsets.all(10.0),
                child: Center(
                  child: Text(
                    futureSnapshot.error.toString(),
                    textAlign: TextAlign.center,
                  ),
                ),
              ),
            ),
          );
        }
        return MaterialApp(
          debugShowCheckedModeBanner: false,
          title: 'ChatInsta',
          theme: ThemeData(
            fontFamily: 'Quicksand',
            primarySwatch: Colors.pink,
            backgroundColor: Colors.pink,
            accentColor: Colors.deepPurpleAccent,
            accentColorBrightness: Brightness.dark,
            elevatedButtonTheme: ElevatedButtonThemeData(
              style: ButtonStyle(
                shape: MaterialStateProperty.all(
                  RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20.0),
                  ),
                ),
              ),
            ),
          ),
          home: SplashScreen(),
        );
      },
    );
  }
}
