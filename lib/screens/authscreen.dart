import 'dart:io';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

import '../screens/chatscreen.dart';
import '../widgets/authform.dart';

class AuthScreen extends StatefulWidget {
  @override
  _AuthScreenState createState() => _AuthScreenState();
}

class _AuthScreenState extends State<AuthScreen> {
  bool _isLoading = false;

  void _userLogin(String email, String password) async {
    setState(() => _isLoading = true);
    final auth = FirebaseAuth.instance;
    try {
      await auth.signInWithEmailAndPassword(email: email, password: password);

      Navigator.of(context)
          .pushReplacement(MaterialPageRoute(builder: (ctx) => ChatScreen()));
          
    } on FirebaseAuthException catch (e) {
      ScaffoldMessenger.of(context).removeCurrentSnackBar();
      ScaffoldMessenger.of(context)
          .showSnackBar(SnackBar(content: Text(e.code)));
      setState(() => _isLoading = false);
    } catch (e) {
      ScaffoldMessenger.of(context)
          .showSnackBar(SnackBar(content: Text(e.toString())));
      setState(() => _isLoading = false);
    }
  }

  void _userSignUp(
      String email, String username, String password, File image) async {
    setState(() => _isLoading = true);
    final auth = FirebaseAuth.instance;
    try {
      final userCredentials = await auth.createUserWithEmailAndPassword(
          email: email, password: password);

      final ref = FirebaseStorage.instance
          .ref()
          .child('user_images')
          .child(userCredentials.user.uid + '.jpg');

      await ref.putFile(image).whenComplete(() => null);

      final imageUrl = await ref.getDownloadURL();

      await userCredentials.user
          .updateProfile(displayName: username, photoURL: imageUrl);

      await FirebaseFirestore.instance.collection('chat').add({
        'Timestamp': Timestamp.now(),
        'Uid': userCredentials.user.uid,
        'Newuser': '$username joined the room',
        'Userimage': imageUrl,
      });

      Navigator.of(context)
          .pushReplacement(MaterialPageRoute(builder: (ctx) => ChatScreen()));

    } on FirebaseAuthException catch (e) {
      ScaffoldMessenger.of(context).removeCurrentSnackBar();
      ScaffoldMessenger.of(context)
          .showSnackBar(SnackBar(content: Text(e.code)));
      setState(() => _isLoading = false);
    } catch (e) {
      ScaffoldMessenger.of(context)
          .showSnackBar(SnackBar(content: Text(e.toString())));
      setState(() => _isLoading = false);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      body: AuthForm(
        userLogin: _userLogin,
        userSignUp: _userSignUp,
        isLoading: _isLoading,
      ),
    );
  }
}
